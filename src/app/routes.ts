import { Routes } from '@angular/router';
import { CreateComponent } from './modules/list/create/create.component';
import { ReadComponent } from './modules/list/read/read.component';
import { AuthComponent } from './modules/authentification/auth/auth.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';

export const appRoutes: Routes = [
  { path: '', redirectTo: 'create', pathMatch: 'full' },
  { path: 'create', component: CreateComponent },
  { path: 'list/:id', component: ReadComponent },
  { path: 'auth', component: AuthComponent },
  { path: 'dashboard', component: DashboardComponent }
];
