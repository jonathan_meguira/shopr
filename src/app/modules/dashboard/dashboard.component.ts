import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ShoppingList } from '../../types/shopping-list.type';
import { ShoprQuery, ShoprService, ShoprState } from '../../state';
import { Observable } from 'rxjs/index';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit {
  uid: string;
  lists$: Observable<ShoppingList[]>;
  noList: boolean;
  hideLoader = false;

  constructor(private query: ShoprQuery,
    private router: Router,
    private shoprService: ShoprService) {

  }

  ngOnInit() {
    this.shoprService.fetchUserLists();
    this.fetchUserList();
  }

  fetchUserList() {
    this.lists$ = this.query.select((state: ShoprState) => state.lists);
  }

  newList() {
    this.router.navigate(['create']);
  }

  duplicate(id: string) {
    this.router.navigate(['create'], { queryParams: { id } });
  }

  get hasNoList() {
    return this.noList;
  }

  showList(id: string) {
    this.router.navigate(['list', id]);
  }



}
