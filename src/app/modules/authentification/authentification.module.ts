import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthComponent} from './auth/auth.component';
import {FormsModule} from '@angular/forms';
import {AngularFireAuthModule} from 'angularfire2/auth';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AngularFireAuthModule
  ],
  declarations: [AuthComponent]
})
export class AuthentificationModule {
}
