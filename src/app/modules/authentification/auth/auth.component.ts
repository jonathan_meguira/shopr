import {Component, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import * as firebase from 'firebase';
import {PhoneNumber} from '../../../types/phone';
import {Router} from '@angular/router';
import {ShoprDataService} from '../../../state';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthComponent {
  phone: string;
  windowRef: any;
  hideCaptcha: boolean;
  verificationCode: string;


  constructor(private dataService: ShoprDataService,
              private cdr: ChangeDetectorRef,
              private router: Router) {
  }


  authenticate() {
    this.makeCaptcha().then(() => {
      const phone = new PhoneNumber().formatPhone(this.phone);
      this.dataService.authenticate(phone, this.windowRef['recaptchaVerifier'])
        .then((data) => {
          this.hideCaptcha = true;
          this.windowRef.confirmationResult = data;
          this.cdr.detectChanges();
        });
    });
  }

  makeCaptcha() {
    this.windowRef = window;
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    return this.windowRef['recaptchaVerifier'].render();
  }

  verifyCode() {
    this.windowRef['confirmationResult']
      .confirm(this.verificationCode)
      .then(result => {
        this.dataService.setKey('s-uid', result.user.uid);
        this.router.navigate(['dashboard']);
      });
  }

}
