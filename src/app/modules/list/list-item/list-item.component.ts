import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { ShoppingItem } from '../../../types/shopping-list.type';

@Component({
  selector: 'app-item-card',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {
  @Input() item: ShoppingItem;
  @Input() readonly: boolean;
  authorMode = false;
  @Output() deleteItem = new EventEmitter();
  @Output() toggle = new EventEmitter<boolean>();
  quantities = Array.from(Array(20).keys());

  ngOnInit() {
    this.quantities.shift();
  }

  toggleStatus({ target }) {
    this.toggle.emit(target.checked);
  }

  delete() {
    this.deleteItem.emit();
  }

}
