import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListItemComponent } from './list-item/list-item.component';
import { ReadComponent } from './read/read.component';
import { FormsModule } from '@angular/forms';
import { CreateComponent } from './create/create.component';

const publicApi = [ReadComponent, ListItemComponent, CreateComponent]

@NgModule({
  declarations: publicApi,
  exports: publicApi,
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class ListModule {
}
