import { Component, ChangeDetectionStrategy, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { ShoprService, ShoprQuery, ShoprState } from '../../../state';
import { ActivatedRoute } from '@angular/router';
import { ShoppingItem, ShoppingList } from '../../../types/shopping-list.type';
import { first } from 'rxjs/operators/first';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateComponent implements OnInit {
  list: ShoppingList;
  saved: boolean;
  whatsappTriggered: boolean;
  phoneToShare: string;
  editMode: boolean;
  loadedData: boolean;

  constructor(
    private service: ShoprService,
    private route: ActivatedRoute,
    private query: ShoprQuery,
    private shoprService: ShoprService,
    private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    const listId = this.route.snapshot.queryParams['id'] || null;
    if (listId) {
      this.loadExistingList(listId);
    } else {
      this.list = new ShoppingList();
      this.loadedData = true;
    }
  }

  addItem(): void {
    this.list.items.push(new ShoppingItem());
  }

  deleteItem(action: string, idx: number) {
    this.list.items.splice(idx, 1);
  }

  save() {
    this.saved = true;
    this.service.saveList(this.list).then(null);
  }

  loadExistingList(id: string) {
    this.shoprService.fetchList(id);
    this.query.select((state: ShoprState) => state.list)
      .pipe(first(list => list.id !== undefined))
      .subscribe((list: ShoppingList) => {
        if (list.items) {
          this.list = list;
          this.list.id = Math.random().toString(36).substring(7);
          this.loadedData = true;
          this.saved = false;
          this.cdr.detectChanges();
        }
      });
  }

  get disableAdding() {
    return this.list.items.filter((item: ShoppingItem) => item.label.length === 0).length > 0;
  }

  get canShare(): boolean {
    return this.saved && !this.disableAdding;
  }

  get isWhatsapp(): boolean {
    return this.whatsappTriggered;
  }

  showList() {
    const link = `localhost:4200/list/${this.list.id}`;
    window.open(link, 'blank');
  }

  share() {
    this.whatsappTriggered = true;
  }

  cancelWhatsapp() {
    this.whatsappTriggered = false;
  }

  openWhatsapp() {
    const link = `localhost:4200/list/${this.list.id}`;
    window.open(`https://api.whatsapp.com/send?phone=${this.phoneToShare}&text=here is your shopping list ${link}`);
    this.list.destinationPhone = this.phoneToShare;
    this.save();

    this.whatsappTriggered = false;
  }

  get hasLoadedData(): boolean {
    return this.loadedData;
  }




}
