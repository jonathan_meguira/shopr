import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShoppingList, GPS } from '../../../types/shopping-list.type';
import { ShoprQuery, ShoprService, ShoprState } from '../../../state';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-list',
  template: `
    <div *ngFor="let item of (list$ | async).items; let idx = index" class="mb-2">
      <app-item-card [item]="item" [readonly] = "true" (toggle)="toggleStatus($event, idx)"></app-item-card>
    </div>
  `,
  styleUrls: ['./read.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReadComponent implements OnInit {
  list$: Observable<ShoppingList>;
  listId: string;
  hasChecked = false;
  readonly = true;
  // TODO: when user get's from dashboard, add a mode where he can edit the list, fetch this param from url;
  constructor(
    private route: ActivatedRoute,
    private query: ShoprQuery,
    private shoprService: ShoprService) {
  }

  ngOnInit() {
    this.listId = this.route.snapshot.params['id'];
    this.shoprService.fetchList(this.listId);
    this.list$ = this.query.select((state: ShoprState) => state.list);
  }

  toggleStatus(purchased: boolean, index: number) {
    if (!this.hasChecked) {
      this.fetchGPS();
      this.hasChecked = true;
    }
    this.shoprService.toggleItemStatus(this.listId, purchased, index);
  }

  fetchGPS() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        const coordinates = new GPS(position);
        this.shoprService.setUserPosition(this.listId, coordinates);
      });
    }
  }
}
