import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {ShoprDataService} from './shopr-data.service';
import {ShoprService} from './shopr.service';
import {environment} from '../../environments/environment';


@NgModule({
  imports: [
    CommonModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule
  ],
  providers: [ShoprDataService, ShoprService]
})
export class ServiceModule {
}
