import {Injectable} from '@angular/core';
import {EntityState, EntityStore} from '@datorama/akita';
import {ShoprState} from './shopr.model';

export interface State extends EntityState<ShoprState> {
}


/**
 * 1 entity store that holds 1 table of list
 * ShoppingList {
 * {id, quantity, date, items}
 *  id, items []ID
 * }
 * {total, date, listId}
 * 
 * 
 */


@Injectable()
export class ShoppingListsStore extends EntityStore<State, ShoprState> {
  constructor() {
    super();
  }

}

/**
 * SHoppingItem {
 * id, label, purchased, quantity
 * }
 */

export class ShoppingItemStore extends EntityStore<State, ShoprState> {
  constructor() {
    super();
  }

}