import { Injectable } from '@angular/core';
import { GPS, ShoppingList } from '../types/shopping-list.type';
import { Observable } from 'rxjs/index';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class ShoprDataService {

  constructor(private db: AngularFireDatabase, private afAuth: AngularFireAuth) {
  }

  fetchList(id: string): Observable<ShoppingList> {
    return this.db.object<ShoppingList>(`${this.uid}/${id}`).valueChanges();
  }

  saveList(list: ShoppingList) {
    list.date = Date.now();
    return this.db.object(`${this.uid}/${list.id}`).set(list);
  }

  authenticate(phoneNumber: string, applicationVerifier): Promise<any> {
    return this.afAuth.auth.signInWithPhoneNumber(phoneNumber, applicationVerifier);
  }

  fetchUserLists(): Observable<ShoppingList[]> {
    return this.db.list<ShoppingList>(this.uid).valueChanges();
  }

  toggleItemStatus(listId: string, purchased: boolean, itemIdx: number) {
    this.db.object(`${this.uid}/${listId}/items/${itemIdx}`).update({ purchased });
  }
  setUserPosition(listId: string, coordinates: GPS) {
    this.db.object(`${this.uid}/${listId}`).update({ coordinates });
  }

  setKey(key: string, value: string) {
    window.localStorage.setItem(key, value);
  }

  get uid(): string {
    return window.localStorage.getItem('s-uid');
  }

}
