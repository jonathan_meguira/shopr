import {Injectable} from '@angular/core';
import {QueryEntity} from '@datorama/akita';
import {ShoprStore, State} from './shopr.store';
import {ShoprState} from './shopr.model';

@Injectable()
export class ShoprQuery extends QueryEntity<State, ShoprState> {

  constructor(protected store: ShoprStore) {
    super(store);
  }

}
