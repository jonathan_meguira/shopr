import { Injectable } from '@angular/core';
import { ShoprStore } from './shopr.store';
import { ShoprDataService } from './shopr-data.service';
import { ShoppingList, GPS } from '../types/shopping-list.type';
import { ShoprState } from './shopr.model';

@Injectable()
export class ShoprService {

  constructor(private shoprStore: ShoprStore,
    private shoprDataService: ShoprDataService) {
  }

  fetchUserLists() {
    this.shoprDataService.fetchUserLists()
      .subscribe((lists: ShoppingList[]) => this.setUserList(lists));
  }

  fetchList(id: string) {
    return this.shoprDataService.fetchList(id)
      .subscribe((list: ShoppingList) => this.setFetchedList(list));
  }

  saveList(list: ShoppingList) {
    return this.shoprDataService.saveList(list);
  }

  authenticate(phoneNumber: string, applicationVerifier) {
    return this.shoprDataService.authenticate(phoneNumber, applicationVerifier);
  }

  toggleItemStatus(listId: string, purchased: boolean, itemIdx: number) {
    //todo : update the store
    this.shoprDataService.toggleItemStatus(listId, purchased, itemIdx);
  }
  setUserPosition(listId: string, coordinates: GPS) {
    this.shoprDataService.setUserPosition(listId, coordinates);
  }
  private setUserList(lists: ShoppingList[]) {
    this.shoprStore.setState((state: ShoprState) => {
      return {
        ...state, lists
      };
    });
  }
  private setFetchedList(list: ShoppingList) {
    this.shoprStore.setState((state: ShoprState) => {
      return {
        ...state, list
      };
    });
  }

}
