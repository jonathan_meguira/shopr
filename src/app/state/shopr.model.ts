import { ID } from '@datorama/akita';

/**
 * scenario: everytime I click on a list to visualize it,
 *  I fetch the list from firebase, and add the fetchlist to the second store
 * add it's id to the frist store into the fetchedIds array.
 * so that if I come back to the same list,
 *  I know I already have fetched it and I don't need to fetch it from firebase again
 */

export interface ShoppingList {
  date: number;
  listId: ID;
  total: number;
  fetchedIds: ID[];
}

export function createShoppingList({ date, listId, total }: ShoppingList) {
  return {
    date, listId, total, fetchedIds: []
  } as ShoppingList;
}



export interface ShoppingItem {
  quantity: number;
  label: string;
  purchased: boolean;
}

export function createShoppingItem({ quantity, label }: Partial<ShoppingItem>) {
  return {
    quantity, label, purchased: false
  } as ShoppingItem;
}