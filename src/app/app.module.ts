import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { ListModule } from './modules/list/list.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { appRoutes } from './routes';
import { AuthentificationModule } from './modules/authentification/authentification.module';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { ServiceModule } from './state/services.module';
import { ShoprQuery, ShoprStore } from './state';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ListModule,
    AuthentificationModule,
    DashboardModule,
    ServiceModule,
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  bootstrap: [AppComponent],
  providers: [ShoprQuery, ShoprStore]
})
export class AppModule {
}
