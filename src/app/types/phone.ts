export class PhoneNumber {
  phone: string;

  formatPhone(phone: string): string {
    let formattedPhone = '972';
    if (phone.charAt(0) === '0') {
      formattedPhone += phone.substring(1);
    } else {
      formattedPhone += phone;
    }
    return `+${formattedPhone}`;
  }
}
