import {SnapshotAction} from 'angularfire2/database';

export class FireDBListMapper<T> {
  data: T[];

  constructor(params: SnapshotAction<T>[]) {
    this.data = [];
    params.forEach(param => this.data.push(param.payload.val()));
  }
}
