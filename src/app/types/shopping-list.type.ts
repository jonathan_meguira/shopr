export class ShoppingList {
  items: ShoppingItem[];
  id: string;
  destinationPhone?: string;
  coordinates?: GPS;
  date: number;

  constructor(items?: ShoppingItem[], id?: string) {
    this.items = items || [new ShoppingItem('')];
    this.id = id || Math.random().toString(36).substring(7);
  }
}

export class ShoppingItem {
  label: string;
  quantity: number;
  purchased: boolean;

  constructor(label: string = '', quantity: number = 1) {
    this.purchased = false;
    this.label = label;
    this.quantity = quantity;
  }
}


export class GPS {
  latitude: number;
  longitude: number;
  constructor(position) {
    this.latitude = position.coords.latitude;
    this.longitude = position.coords.longitude;
  }
}