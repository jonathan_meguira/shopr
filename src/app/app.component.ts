import {Component, ChangeDetectionStrategy} from '@angular/core';

import {Router} from '@angular/router';
import {ShoprDataService, ShoprService} from './state';

@Component({
  selector: 'app-root',
  template: `
    <div class="prime-container" layout="column" layout-align="start center">
      <img src="../assets/icons/logo.svg" class="m-5"/>
      <router-outlet></router-outlet>
    </div>
  `,
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  constructor(private dataService: ShoprDataService, private router: Router) {
    if (!this.dataService.uid) {
      this.router.navigate(['auth']);
    }
  }
}

